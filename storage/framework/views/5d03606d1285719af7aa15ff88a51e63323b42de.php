<?php $__env->startSection('content'); ?>
<div class="container-fluid">
	<h3>Create Category</h3>
	<div class="row">
		<div class="col-lg-6">
        <?php if(empty($edit)): ?>
        <?php echo e(Form::open(array('url' => '/admin/test/category/create'))); ?>

        <?php else: ?>
        <?php echo e(Form::open(array('url' => '/admin/test/category/edit/'.$edit->id))); ?> 
        <?php endif; ?>

				<div class="form-group">
					<label>Create Category</label>
					<input type ="text" class="form-control" name ="name" value ="<?php if(!empty($edit)): ?><?php echo e($edit->name); ?> <?php endif; ?>" required>
					<p class="help-block"></p>
				</div>
				
				<button type="submit" class="btn btn-primary">Submit</button>
				<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
			</form>
		</div>
	</div>
</div>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daffodealsstudy\resources\views/admin/survey/category/create.blade.php ENDPATH**/ ?>