<?php $__env->startSection('content'); ?>

<div class="container-fluid">
   <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Edit Question or Update Corrrect answer</h1>
                  </div>
                  
                  <?php echo Form::open(['url' => '/admin/survey/editquestion/'.$question['id'] ,'class' => 'user','files' => true]); ?>    
                    <div class="form-group">
                      <input type="text" name ="title" class="form-control form-control-user" id="exampleInputEmail" value ="<?php echo e($question['title']); ?>">
                    </div>
                    <div class="form-group">
                    <?php if(!empty($question['option_name']) && $question['question_type'] == 'radio'): ?>
                 
                    <?php $__currentLoopData = explode(',',$question->option_name); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="form-group">
                          <input type="radio" name = "correct_answer" id="<?php echo e($key); ?>" value ="<?php echo e($key); ?>" 
                          <?php if(isset($question['correct_answer']) && $question['correct_answer'] == $key): ?> checked <?php endif; ?> />
                          <label for="<?php echo e($key); ?>"><?php echo e($value); ?></label>
                    </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                    <?php elseif(isset($question['option_name']) && $question['question_type'] == 'checkbox'): ?>
                    <?php $__currentLoopData = explode(',',$question->option_name); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="form-group">
                          <input type="checkbox" name = "correct_answer" id="<?php echo e($key); ?>" value ="<?php echo e($key); ?>"
                          
                          <?php if(!empty($question['correct_answer']) && $question['correct_answer'] == $key): ?> checked <?php endif; ?> />
                          <label for="<?php echo e($key); ?>"><?php echo e($value); ?></label>
                    </div>

                   

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php elseif(empty($question['option_name']) && $question['question_type'] == 'file_listening'): ?>

                    <div class="form-group">
                          <input type="file" name = "correct_answers" id="" value ="" accept="audio/*" />
                          <label for="">Upload File</label>
                    </div>
                    <?php endif; ?>
                      
                      <input  type ="submit" class="btn btn-primary btn-user btn-block" value ="Update">                   
                    
                    </a>
                    
                </div>
              </form>
              </div>
 </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daffodealsstudy\resources\views/admin/survey/question/edit.blade.php ENDPATH**/ ?>