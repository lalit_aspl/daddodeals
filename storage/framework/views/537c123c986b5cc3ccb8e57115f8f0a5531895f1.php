<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?php echo e(__('Enter Otp Number')); ?></div>

                </br>
        <?php if(!empty($message)): ?>
        <div class="alert alert-success ">
        <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>  -->
        <p class ="otpmessage"> <?php echo e($message); ?> </p>
        </div>
        <?php endif; ?>
       


                <form method="POST" id="submit_form" action="<?php echo e(url('/verifyotp')); ?>" id="register-form"  style="display: block;">
									<?php echo csrf_field(); ?>

                                    <input type ="hidden" name ="phone" value ="<?php echo e($phone); ?>">                                    
                                    <input type ="hidden" name ="time" value ="<?php echo e($time); ?>">
                                   
                                   <div class="form-group row">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Otp')); ?></label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="text" class="form-control" name="otp" placeholder ="Enter Otp Number" required >
                                    <br>
                                <button type="button" class="btn btn-primary checkotp">
                                    Verify
                                </button>
                          
                                <button type="button" onClick="window.location.reload();" class="btn btn-primary resendotp">
                                    Resend OTP
                                </button>
                            </div>
                        </div> 
								</form>
              
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daffodealsstudy\resources\views/Auth/verify.blade.php ENDPATH**/ ?>