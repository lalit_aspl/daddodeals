<?php $__env->startSection('content'); ?>
<div class="container-fluid">
	<h3>Create SubCategory</h3>
	<div class="row">
		<div class="col-lg-6">
        <?php if(empty($edit)): ?>
        <?php echo e(Form::open(array('url' => '/admin/test/subcategory/create'))); ?>

        <?php else: ?>
        <?php echo e(Form::open(array('url' => '/admin/test/subcategory/edit/'.$edit->id))); ?> 
        <?php endif; ?>

				<div class="form-group">
					<label>Name</label>
					<input type ="text" class="form-control" name ="name" value ="<?php if(!empty($edit)): ?><?php echo e($edit->name); ?> <?php endif; ?>" required>
				          	<?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>   
				</div>
				

                <div class="form-group">
                <?php echo e(Form::label('category_id', 'Category :')); ?>

				<?php echo Form::select('category_id', $data,  empty($edit) ? null : $edit->category_id, ['class' => 'form-control subdata' , 'placeholder' => 'Please select ...',]); ?>				<?php $__errorArgs = ['category_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>   
				</div>

				
				<button type="submit" class="btn btn-primary">Submit</button>
				<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
			</form>
		</div>
	</div>
</div>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daffodealsstudy\resources\views/admin/survey/subcategory/create.blade.php ENDPATH**/ ?>