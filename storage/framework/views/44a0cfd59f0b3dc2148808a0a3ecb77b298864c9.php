<?php $__env->startSection('content'); ?>



<div class="container-fluid">
	<h3>Create Test</h3>
	<div class="row">
		<div class="col-lg-6">
                        <?php if(empty($edit)): ?>
                           <?php echo Form::open(['url' => '/admin/survey/new']); ?>

                         <?php else: ?>
                         <?php echo Form::open(['url' => '/admin/survey/edit/'.$edit['id']]); ?>

                         <?php endif; ?>

				<div class="form-group">
					<label>Name</label>
                    <input name="title" class="form-control " id="title" type="text" value = "<?php if(!empty($edit['title'])): ?> <?php echo e($edit['title']); ?> <?php endif; ?>" required>
                             <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>           
				</div>

                <div class="form-group">
                <?php echo e(Form::label('testtype_id', 'Type :')); ?>

                <?php echo Form::select('testtype_id', $type,  empty($edit) ? null : '$edit->testtype_id' , ['class' => 'form-control ' , 'placeholder' => 'Please select ...',]); ?>

                          
                          <?php $__errorArgs = ['testtype_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> 
                </div>

            
                <div class="form-group">
                <?php echo e(Form::label('category_id', 'Category :')); ?>

                <?php echo Form::select('category_id', $data,  empty($edit) ? null : $edit->category_id, ['class' => 'form-control subdata' , 'placeholder' => 'Please select ...',]); ?>

                          
                          <?php $__errorArgs = ['category_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> 
                </div>

                <div class="form-group">
                <label for="category_id">Sub Category</label>
                <select class="form-control" id="subcategory" name="subcategory_id" > 
                <?php if(!empty($edit)): ?>) subcategory
                <option value="<?php echo e($edit->subcategory->id); ?>"><?php echo e($edit->subcategory->name); ?></option>
                <?php else: ?>
                <option value="">Please select Category ...</option>
                <?php endif; ?>
                            
                </select>
                <?php $__errorArgs = ['subcategory_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>

				
				<button type="submit" class="btn btn-primary">Submit</button>
				<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
			</form>
		</div>
	</div>
</div>
 <?php $__env->stopSection(); ?>

 <?php $__env->startSection('javascript'); ?>
 <script>


    $(document).on("change", ".subdata", function(event){
        event.preventDefault();
   var id = $(this).val();   
   var url = "<?php echo e(url('/admin/survey/new/subcat')); ?>";

 $.ajax({
         type:'POST',
         url:url,
         data: {
        "_token": "<?php echo e(csrf_token()); ?>",
        "id": id
        },
         success:function(data){
            $('#subcategory').empty();
            $('#subcategory').append(data);
           //console.log(data);
         }
      });
});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daffodealsstudy\resources\views/admin/survey/new.blade.php ENDPATH**/ ?>