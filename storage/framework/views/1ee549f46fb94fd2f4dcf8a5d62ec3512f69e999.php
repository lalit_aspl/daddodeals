<?php $__env->startSection('content'); ?>

       <!-- Table -->



        <div class="container-fluid">

<!-- Page Heading -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Create Test </h6>
              <a href ="<?php echo e(url('/admin/survey/new')); ?>" class ="btn btn-primary" style ="float:right">Create New</a>
 
            </div>           
           
        
            <?php if(session()->has('message')): ?>
          
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong><?php echo e(session()->get('message')); ?>

            </div>
            <?php endif; ?>

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Test Type</th>
                      <th>Category</th>
                      <th>SubCategory</th>
                      <th>Action</th> 
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Test Type</th>
                      <th>Category</th>
                      <th>SubCategory</th>
                      <th>Action</th>                    
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php $i = 1; ?>
                  <?php $__empty_1 = true; $__currentLoopData = $surveys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                      <td><?php echo e($i++); ?></td>
                      <td><?php echo e($name->title); ?></td>
                      <td><?php echo e($name->type->name); ?></td>
                      <td><?php echo e($name->category->name); ?></td>
                      <td><?php echo e($name->subcategory->name); ?></td>
                      <td>
                      
                      <a href ="<?php echo e(url('/admin/survey/view/'.$name->id)); ?>" class ="btn btn-primary">View</a>
                       <a href ="<?php echo e(url('/admin/survey/edit/'.$name->id)); ?>" class ="btn btn-info">Edit</a>
                       <a href ="<?php echo e(url('/admin/survey/deletesurvey/'.$name->id)); ?>" class ="btn btn-danger" onclick="return confirm('Are you sure you want to Delete?');">Delete</a>  </td>
                   </tr>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <span class='flow-text center-align'>Nothing to show</span>
          <?php endif; ?>

                    <!-- //Close -->
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daffodealsstudy\resources\views/admin/survey/index.blade.php ENDPATH**/ ?>