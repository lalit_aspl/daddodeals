@extends('admin.layouts.master')
@section('content')


<div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Test</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div>

</div>

    <div class="row">

<div class="container-fluid bg-info">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h3><span class="label label-warning" id="qid"></span> {{$questions->title}}</h3>
        </div>
        <div class="modal-body">
            <div class="col-xs-3 col-xs-offset-5">
               <div id="loadbar" style="display: none;">
                  <div class="blockG" id="rotateG_01"></div>
                  <div class="blockG" id="rotateG_02"></div>
                  <div class="blockG" id="rotateG_03"></div>
                  <div class="blockG" id="rotateG_04"></div>
                  <div class="blockG" id="rotateG_05"></div>
                  <div class="blockG" id="rotateG_06"></div>
                  <div class="blockG" id="rotateG_07"></div>
                  <div class="blockG" id="rotateG_08"></div>
              </div>
          </div>
         
         @if($questions->question_type == 'radio' && !empty($questions))
         @foreach(explode(',',$questions->option_name) as $key=>$value)
           <div class="quiz" id="quiz" data-toggle="buttons">
           <label class="element-animation1 btn btn-lg btn-primary btn-block">
           <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> 
           <input type="radio" name="q_answer" value="1">{{ $value }}</label>           
          
       </div>
        @endforeach
        @elseif($questions->question_type == 'checkbox')

        @foreach(explode(',',$questions->option_name) as $key=>$value)
           <div class="quiz" id="quiz" data-toggle="buttons">
           <label class="element-animation1 btn btn-lg btn-primary btn-block">
           <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> 
           <input type="radio" name="q_answer" value="1">{{ $value }}</label>           
          
       </div>
        @endforeach
        @elseif($questions->question_type == 'text')

        <div class="quiz" id="quiz" data-toggle="buttons">
           <label class="element-animation1 btn btn-lg btn-primary btn-block">
           <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>            
           <textarea name ="q_answer" placeholder="Write your answer here..." > </textarea>    
          
       </div>
        @else

        No  Data Found

        @endif
   </div>
   <div class="modal-footer text-muted">
    <span id="answer"></span>
</div>
</div>
</div>
</div>

</div>

@endsection