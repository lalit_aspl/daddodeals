@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div>

</div>


         @if(session()->has('message'))
          
          <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong>Alert!</strong>{{ session()->get('message') }}
          </div>
          @endif

    <div class="row">

        
    @forelse ($tests as $user)
    <!-- Earnings (Monthly) Card Example -->
    <div class="col-md-6">
    <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1"> {{$user->type->name}} </div>
            <div class="row no-gutters align-items-center">
                <div class="col-auto">
                <!-- <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div> -->
                </div>
                <div class="col">
                <!-- <div class="progress progress-sm mr-2">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div> -->
                <h4><a href ="{{url('/user/exam/'.$user->id)}}"> {{$user->title}} </a> </h4>
                </div>
            </div>
            </div>
            <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
            </div>
        </div>
        </div>
    </div>
    </div>
    @empty

    No users found.


@endforelse

</div>


@endsection
@section('javascript')
@stop