@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Enter Otp Number') }}</div>

                </br>
        @if(!empty($message))
        <div class="alert alert-success ">
        <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>  -->
        <p class ="otpmessage"> {{ $message }} </p>
        </div>
        @endif
       


                <form method="POST" id="submit_form" action="{{url('/verifyotp')}}" id="register-form"  style="display: block;">
									@csrf

                                    <input type ="hidden" name ="phone" value ="{{$phone}}">                                    
                                    <input type ="hidden" name ="time" value ="{{$time}}">
                                   
                                   <div class="form-group row">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Otp') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="text" class="form-control" name="otp" placeholder ="Enter Otp Number" required >
                                    <br>
                                <button type="button" class="btn btn-primary checkotp">
                                    Verify
                                </button>
                          
                                <button type="button" onClick="window.location.reload();" class="btn btn-primary resendotp">
                                    Resend OTP
                                </button>
                            </div>
                        </div> 
								</form>
              
            </div>
        </div>
    </div>
</div>

@endsection


