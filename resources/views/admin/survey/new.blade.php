@extends('admin.layouts.master')
@section('content')



<div class="container-fluid">
	<h3>Create Test</h3>
	<div class="row">
		<div class="col-lg-6">
                        @if(empty($edit))
                           {!! Form::open(['url' => '/admin/survey/new']) !!}
                         @else
                         {!! Form::open(['url' => '/admin/survey/edit/'.$edit['id']]) !!}
                         @endif

				<div class="form-group">
					<label>Name</label>
                    <input name="title" class="form-control " id="title" type="text" value = "@if(!empty($edit['title'])) {{$edit['title']}} @endif" required>
                             @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror           
				</div>

                <div class="form-group">
                {{ Form::label('testtype_id', 'Type :')}}
                {!! Form::select('testtype_id', $type,  empty($edit) ? null : '$edit->testtype_id' , ['class' => 'form-control ' , 'placeholder' => 'Please select ...',]) !!}
                          
                          @error('testtype_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                </div>

            
                <div class="form-group">
                {{ Form::label('category_id', 'Category :')}}
                {!! Form::select('category_id', $data,  empty($edit) ? null : $edit->category_id, ['class' => 'form-control subdata' , 'placeholder' => 'Please select ...',]) !!}
                          
                          @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                </div>

                <div class="form-group">
                <label for="category_id">Sub Category</label>
                <select class="form-control" id="subcategory" name="subcategory_id" > 
                @if(!empty($edit))) subcategory
                <option value="{{$edit->subcategory->id}}">{{$edit->subcategory->name}}</option>
                @else
                <option value="">Please select Category ...</option>
                @endif
                            
                </select>
                @error('subcategory_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                </div>

				
				<button type="submit" class="btn btn-primary">Submit</button>
				<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
			</form>
		</div>
	</div>
</div>
 @endsection

 @section('javascript')
 <script>


    $(document).on("change", ".subdata", function(event){
        event.preventDefault();
   var id = $(this).val();   
   var url = "{{url('/admin/survey/new/subcat')}}";

 $.ajax({
         type:'POST',
         url:url,
         data: {
        "_token": "{{ csrf_token() }}",
        "id": id
        },
         success:function(data){
            $('#subcategory').empty();
            $('#subcategory').append(data);
           //console.log(data);
         }
      });
});

</script>
@stop