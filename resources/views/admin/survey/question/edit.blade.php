@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
   <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Edit Question or Update Corrrect answer</h1>
                  </div>
                  
                  {!! Form::open(['url' => '/admin/survey/editquestion/'.$question['id'] ,'class' => 'user','files' => true]) !!}    
                    <div class="form-group">
                      <input type="text" name ="title" class="form-control form-control-user" id="exampleInputEmail" value ="{{$question['title']}}">
                    </div>
                    <div class="form-group">
                    @if(!empty($question['option_name']) && $question['question_type'] == 'radio')
                 
                    @foreach(explode(',',$question->option_name) as $key=>$value)
                    <div class="form-group">
                          <input type="radio" name = "correct_answer" id="{{ $key }}" value ="{{ $key }}" 
                          @if(isset($question['correct_answer']) && $question['correct_answer'] == $key) checked @endif />
                          <label for="{{ $key }}">{{ $value }}</label>
                    </div>
                      @endforeach
                    
                    @elseif(isset($question['option_name']) && $question['question_type'] == 'checkbox')
                    @foreach(explode(',',$question->option_name) as $key=>$value)
                    <div class="form-group">
                          <input type="checkbox" name = "correct_answer" id="{{ $key }}" value ="{{ $key }}"
                          
                          @if(!empty($question['correct_answer']) && $question['correct_answer'] == $key) checked @endif />
                          <label for="{{ $key }}">{{ $value }}</label>
                    </div>

                   

                    @endforeach

                    @elseif(empty($question['option_name']) && $question['question_type'] == 'file_listening')

                    <div class="form-group">
                          <input type="file" name = "correct_answers" id="" value ="" accept="audio/*" />
                          <label for="">Upload File</label>
                    </div>
                    @endif
                      
                      <input  type ="submit" class="btn btn-primary btn-user btn-block" value ="Update">                   
                    
                    </a>
                    
                </div>
              </form>
              </div>
 </div>
</div>

@endsection