@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">

<div class="row">
    <div class="col-8  offset-md-2">
        <div class="card  p-3">
            <div class="card-content">
                <span class="card-title"> {{$survey['name']}}</span>
                
                <br>
                <!-- <a href="view/4">Take Survey</a> | <a href="4/edit">Edit Survey</a> 
                
                | <a href="/survey/answers/4">View Answers</a> -->
                 <a href="{{url('/admin/survey/deletesurvey/'.request()->segment(3))}}" style="float:right;" class="modal-trigger red-text" onclick="return confirm('Are you sure you want to Delete?');">Delete Survey</a>
                <!-- Modal Structure -->
                <!-- TODO Fix the Delete aspect -->
            
                <div class="divider" style="margin:20px 0px;"></div>
                <h4 class="flow-text text-center">{{$survey['title']}}</h4>

                 <!-- display question  -->
                 <p class="flow-text center-align">Questions</p>

@if( !empty($survey['questions']))
@php  $i = 1;@endphp
  @foreach ($survey['questions'] as $question)
  @php $d = $i++; @endphp
    <div class="accordion" id="accordionExample{{$d}}">
        <div class="card">
            <div class="card-header" data-toggle="collapse" data-target="#collapseOne{{$d}}" aria-expanded="true">     
                <span class="title">{{ $question->title }} 

                @if(!empty( Request::segment(2)) &&  Request::segment(2) != 'view')
                <a  href="{{url('/admin/survey/editquestion/'.$question->id)}}" style="float:right;">
                Edit</a> </br> &nbsp

                <a  href="{{url('/admin/survey/editquestion/deletequestion/'.$question->id)}}" style="float:right;"
                onclick="return confirm('Are you sure you want to Remove?');">
                Delete</a>
                @endif
               </span>
             

               
            </div>

            <div id="collapseOne{{$d}}" class="collapse" data-parent="#accordionExample{{$d}}">
                <div class="card-body">
                <div style="margin:5px; padding:10px;">
                  {!! Form::open() !!}
                    @if($question->question_type === 'text')
                      {{ Form::text('title')}}
                    @elseif($question->question_type === 'textarea')
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="textarea1" class="materialize-textarea"></textarea>
                        
                      </div>
                    </div>                   
                    @elseif($question->question_type === 'file_listening')
                    <div class="row">
                      <div class="input-field col s12">
                      <input type="file" name = "correct_answer" id="{{ $key }}" />
                        
                      </div>
                    </div>
                    @elseif($question->question_type === 'file_speaking')
                    <div class="row">
                      <div class="input-field col s12">
                      <audio controls> </audio>

                         
                      </div>
                    </div>
                    @elseif($question->question_type === 'radio')
                      @foreach(explode(',',$question->option_name) as $key=>$value)
                        <p style="margin:0px; padding:0px;">
                          <input type="radio" name = "correct_answer" id="{{ $key }}" />
                          <label for="{{ $key }}">{{ $value }}</label>
                        </p>
                      @endforeach
                    @elseif($question->question_type === 'checkbox')
                        @foreach(explode(',',$question->option_name) as $key=>$value)
                        <p style="margin:0px; padding:0px;">
                            <input type="checkbox" name ="correct_answer" id="{{ $key }}" />
                            <label for="{{$key}}">{{ $value }}</label>
                        </p>
                        @endforeach
                    @endif 
                    {!! Form::close() !!}
              </div>
                </div>
            </div>
        </div>
      </div>
      @endforeach
      @else
      <span style="padding:10px;">Nothing to show. Add questions below.</span>
      @endif

          
          
            
   
                @if(!empty( Request::segment(2)) &&  Request::segment(2) != 'view')
                <!-- display Question -->
           
                <h3 class="flow-text">Add Question</h3>
             
                {!! Form::open(['url' => '/admin/survey/'.$survey['id']]) !!}                  
                    
                    <div class="col-12">
                        <div class="input-field">
                            <select class="form-control" name="question_type" id="question_type" required>
                                <option value="" disabled="" selected="">Choose your option</option>
                                <option value="text">Text</option>
                                <option value="textarea">Textarea</option>
                                <option value="checkbox">Checkbox</option>
                                <option value="radio">Radio </option>
                                <option value="file_listening">Audio Listening</option>
                                <option value="file_speaking">Audio Speaking</option>
                            </select>
                        </div>
                        </div>
                        <br>

                        
                    <div class="col-12">
                        <div class="input-field">
                        <label for="title">Question</label>
                            <input  class="form-control" name="title" id="title" type="text">
                           
                        </div>
                        <div class="input-field optans" style ="display:none">
                        <label for="title">Option</label>
                         <input class="form-control" name="option_name[]" id="option_name[]" type="text" >
                         </div>
                         </br>
                         <div class="input-field p_scents" style ="display:none" >
                        
                         <input class="btn btn-primary"  id="add_button" type="button" value ="Add another">
                         </div>

                       <hr>
                        <!-- this part will be chewed by script in init.js -->
                        <span class="form-g"></span>
                        <div class="input-field col s12">
                            <button class="btn btn-primary waves-light">Submit</button>
                        </div>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')

<script>
    
    $(document).on('change', '#question_type', function() {
         var q_type = $(this).val();
        
         if(q_type == 'checkbox' || q_type == 'radio')
         {
            $(".optans").css("display", "block");
            $(".p_scents").css("display", "block");
            
         }else{
            $(".optans").css("display", "none");
            $(".p_scents").css("display", "none");
         }
        
  });

  $(document).ready(function(){
    var wrapper = $(".optans");
    //var add_button = $(".add_field_button");

$("#add_button").click(function (e) {
    e.preventDefault();
    $(wrapper).after('<div><label for="title">Option</label><input type="text" name="option_name[]" class="form-control"><a href="#" class="remove_field">Remove</a></div>'); //add input box
});

$(document).on("click",".remove_field",function(){
    $(this).parent().remove();
});
});
  
  </script>

@stop