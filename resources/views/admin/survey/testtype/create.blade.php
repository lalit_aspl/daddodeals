@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<h3>Create Test Type</h3>
	<div class="row">
		<div class="col-lg-6">
        @if(empty($edit))
        {{ Form::open(array('url' => 'admin/test/type/create')) }}
        @else
        {{ Form::open(array('url' => '/admin/test/type/edit/'.$edit->id)) }} 
        @endif

				<div class="form-group">
					<label>Create Test Type</label>
					<input type ="text" class="form-control" name ="name" value ="@if(!empty($edit)){{$edit->name}} @endif" required>
					<p class="help-block"></p>
				</div>
				
				<button type="submit" class="btn btn-primary">Submit</button>
				<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
			</form>
		</div>
	</div>
</div>
 @endsection