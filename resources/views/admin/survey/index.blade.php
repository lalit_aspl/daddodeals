@extends('admin.layouts.master')

@section('content')

       <!-- Table -->



        <div class="container-fluid">

<!-- Page Heading -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Create Test </h6>
              <a href ="{{url('/admin/survey/new')}}" class ="btn btn-primary" style ="float:right">Create New</a>
 
            </div>           
           
        
            @if(session()->has('message'))
          
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong>{{ session()->get('message') }}
            </div>
            @endif

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Test Type</th>
                      <th>Category</th>
                      <th>SubCategory</th>
                      <th>Action</th> 
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Test Type</th>
                      <th>Category</th>
                      <th>SubCategory</th>
                      <th>Action</th>                    
                    </tr>
                  </tfoot>
                  <tbody>
                  @php $i = 1; @endphp
                  @forelse ($surveys as $key=>$name)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$name->title}}</td>
                      <td>{{$name->type->name}}</td>
                      <td>{{$name->category->name}}</td>
                      <td>{{$name->subcategory->name}}</td>
                      <td>
                      
                      <a href ="{{url('/admin/survey/view/'.$name->id)}}" class ="btn btn-primary">View</a>
                       <a href ="{{url('/admin/survey/edit/'.$name->id)}}" class ="btn btn-info">Edit</a>
                       <a href ="{{url('/admin/survey/deletesurvey/'.$name->id)}}" class ="btn btn-danger" onclick="return confirm('Are you sure you want to Delete?');">Delete</a>  </td>
                   </tr>
                   @empty
            <span class='flow-text center-align'>Nothing to show</span>
          @endforelse

                    <!-- //Close -->
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        

@endsection