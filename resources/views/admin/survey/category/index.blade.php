@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">

<!-- Page Heading -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Category</h6>
              <a href ="{{url('/admin/test/category/create')}}" class ="btn btn-primary" style ="float:right">Add Category</a>
 
            </div>           
           


            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Action</th> 
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Action</th>                    
                    </tr>
                  </tfoot>
                  <tbody>
                  @php $i = 1; @endphp
                  @forelse ($category as $key=>$name)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$name->name}}</td>
                      <td><a href ="{{url('/admin/test/category/edit/'.$name->id)}}" class ="btn btn-info">Edit</a>
                       <a href ="{{url('/admin/test/category/delete/'.$name->id)}}" class ="btn btn-danger" onclick="return confirm('Are you sure you want to Delete?');">Delete</a>  </td>
                   </tr>
                   @empty
            <span class='flow-text center-align'>Nothing to show</span>
          @endforelse

                    <!-- //Close -->
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>



@endsection


@section('javascript')


@stop