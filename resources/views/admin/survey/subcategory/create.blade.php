@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<h3>Create SubCategory</h3>
	<div class="row">
		<div class="col-lg-6">
        @if(empty($edit))
        {{ Form::open(array('url' => '/admin/test/subcategory/create')) }}
        @else
        {{ Form::open(array('url' => '/admin/test/subcategory/edit/'.$edit->id)) }} 
        @endif

				<div class="form-group">
					<label>Name</label>
					<input type ="text" class="form-control" name ="name" value ="@if(!empty($edit)){{$edit->name}} @endif" required>
				          	@error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror   
				</div>
				

                <div class="form-group">
                {{ Form::label('category_id', 'Category :')}}
				{!! Form::select('category_id', $data,  empty($edit) ? null : $edit->category_id, ['class' => 'form-control subdata' , 'placeholder' => 'Please select ...',]) !!}				@error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror   
				</div>

				
				<button type="submit" class="btn btn-primary">Submit</button>
				<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
			</form>
		</div>
	</div>
</div>
 @endsection