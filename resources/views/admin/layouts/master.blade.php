<!DOCTYPE html>
<html lang="en">

<head>

    @include('admin.partials.header')

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    @include('admin.partials.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
    @include('admin.partials.topbar')
    @yield('content')

  </div>
  @include('admin.partials.footer')
 </div>
 </div>
 @include('admin.partials.modal')
 @include('admin.partials.javascripts')
 @yield('javascript')
</body>