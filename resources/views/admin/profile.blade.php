@extends('admin.layouts.master')
@section('content')


<div class="container">

@if (session('message'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }}
        </div>
        @endif
  <h2>Edit Profile</h2>
  {{ Form::open(array('url' => '/admin/profile' , 'files' => true)) }}

 <input type ="hidden" name ="id" value= "{{auth()->user()->id}}" >
    <div class="form-group">
      <label for="email">Name:</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" id="email" placeholder="Enter Name" name="name" value = "{{auth()->user()->name}}">
      @error('name')
          <span class="invalid-feedback" role="alert">
                   <strong>{{ $message }}</strong>
               </span>
            @enderror
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter email" name="email" value = "{{auth()->user()->email}}">
      @error('email')
          <span class="invalid-feedback" role="alert">
                   <strong>{{ $message }}</strong>
               </span>
            @enderror
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control @error('password') is-invalid @enderror" id="pwd" placeholder="Enter password" name="password" >
      @error('password')
          <span class="invalid-feedback" role="alert">
                   <strong>{{ $message }}</strong>
               </span>
            @enderror
    </div>

    <div class="form-group">
      <label for="pwd">Phone:</label>
      <input type="text" class="form-control @error('phone') is-invalid @enderror" id="pwd" placeholder="Phone" name="phone" value = "{{auth()->user()->phone}}">
      @error('phone')
          <span class="invalid-feedback" role="alert">
                   <strong>{{ $message }}</strong>
               </span>
            @enderror
    </div>

    <div class="form-group">
      <label for="pwd">Image:</label>
      <input type="file" class="form-control @error('image') is-invalid @enderror" id="pwd" placeholder="Enter password" name="images" accept="image/x-png,image/gif,image/jpeg">
    </div>
    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>


@endsection