@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">

<!-- Page Heading -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Users</h6>            
 
            </div>           
           


            <div class="card-body">
              <div class="table-responsive">
              <table id="example4" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>id</th>  
                  <th>Name</th>  
                  <th>Email</th> 
                  <th>Phone</th>  
                  <th>Last Login</th>                
                  <th> Action </th>
                
                  
                </tr>
                </thead>
                <tbody>
                          
                </tbody>
                
              </table>
              </div>
            </div>
          </div>



@endsection


@section('javascript')
<script type="text/javascript" language="javascript" >

  $(document).ready( function () {
    $('#example4').DataTable({
           processing: true,
           serverSide: true,
           ajax: "{{ url('/admin/users') }}",
           columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},  
            {data: 'last_login', name: 'last_login'},       
            {data: 'action',  name: 'action',orderable: false, searchable: false},

           
        ]
        });
     });
 
  $(document).on('click','.deletuser',function(){
    var id  = $(this).data("id");
     var url = "{{url('/admin/delete/')}}";
    if(confirm('Are you sure to remove this User ?'))
        {
    $.ajax({
  url: url+'/'+id,
  cache: false,
  success: function(html){
    $('#example4').DataTable().draw(false);
  }
});
        }
});
</script>

@stop