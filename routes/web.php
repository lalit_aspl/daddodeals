<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Mobile login

Route::get('/user/login', 'Auth\MobileLoginController@index')->name('user/login');
Route::post('/user/login', 'Auth\MobileLoginController@generateotp');
Route::post('/verifyotp', 'Auth\MobileLoginController@verifyotp');




Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');     
Route::get('/admin/profile', 'ProfileController@profile');
Route::post('/admin/profile', 'ProfileController@update');


//******************** User Route *********************************** //
Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['user'] ,'namespace' => 'User','prefix' =>'user'], function () {
    Route::get('/index/{id}', ['as' => 'admin.user.index', 'uses' => 'UserController@index']);
    Route::get('/exam/{id}', ['as' => 'admin.user.index', 'uses' => 'UserController@exam']);
    
  
});




//******************** Admin Route *********************************** //
 Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['admin'] ], function () { 

          Route::group(['namespace' => 'Admin\Test','prefix' =>'admin'], function () {
          Route::get('/question/create', 'QuestionController@create');          
          
          Route::get('/survey/index', 'SurveyController@index');
          Route::get('/survey/new', 'SurveyController@new');
          Route::post('/survey/new', 'SurveyController@save');
          Route::post('/survey/new/subcat', 'SurveyController@subcat');
          Route::get('/survey/{survey}', 'SurveyController@survey');
          Route::get('/survey/edit/{survey}', 'SurveyController@edit');
          Route::post('/survey/edit/{survey}', 'SurveyController@update');
          Route::get('/survey/view/{survey}', 'SurveyController@survey');
          Route::get('/survey/deletesurvey/{survey}', 'SurveyController@deletesurvey');         

          
          Route::post('/survey/{question}', 'SurveyController@savequestion');
          Route::get('/survey/editquestion/{question}', 'SurveyController@editquestion');
          Route::post('/survey/editquestion/{question}', 'SurveyController@updatequestion');
          Route::get('/survey/editquestion/deletequestion/{question}', 'SurveyController@deletequestion'); 
          
          //********************    Test Category *********************************** //
          Route::get('/test/category', 'CategoryController@index'); 
          Route::get('/test/category/create', 'CategoryController@create');
          Route::post('/test/category/create', 'CategoryController@store');
          Route::get('/test/category/edit/{id}', 'CategoryController@edit');
          Route::post('/test/category/edit/{id}', 'CategoryController@update');
          Route::get('/test/category/delete/{id}', 'CategoryController@delete');
       
          //********************    Test SubCategory *********************************** //
          Route::get('/test/subcategory/', 'SubcategoryController@index'); 
          Route::get('/test/subcategory/create', 'SubcategoryController@create');
          Route::post('/test/subcategory/create', 'SubcategoryController@store');
          Route::get('/test/subcategory/edit/{id}', 'SubcategoryController@edit');
          Route::post('/test/subcategory/edit/{id}', 'SubcategoryController@update');
          Route::get('/test/subcategory/delete/{id}', 'SubcategoryController@delete');
          //********************    Test Type *********************************** //
          Route::get('/test/type/index', 'TesttypeController@index'); 
          Route::get('/test/type/create', 'TesttypeController@create');  
          Route::post('/test/type/create', 'TesttypeController@store');
          Route::get('/test/type/edit/{id}', 'TesttypeController@edit');
          Route::post('/test/type/edit/{id}', 'TesttypeController@update');
          Route::get('/test/type/delete/{id}', 'TesttypeController@delete');
          
         });

         //   User Detail
         Route::get('/admin/users', ['as' => 'admin.user.index', 'uses' => 'Admin\UserController@index']);
         Route::get('/admin/userlist', ['as' => 'admin.user.index', 'uses' => 'Admin\UserController@userlist']);
         Route::get('/admin/delete/{id}', ['as' => 'admin.user.index', 'uses' => 'Admin\UserController@delete']);

         



   });
