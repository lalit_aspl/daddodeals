<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','image','role_id','last_login_at','otp','login_status','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function test()
    {
     return $this->hasOne('App\Testtype','id','test_id')->select('id','name');
    }




    public function role() {

        return $this->belongsTo(Role::class);

    }
    public function hasRole($roles)

    {

        $this->have_role = $this->getUserRole();

        // Check if the user is a root account

        if($this->have_role->title == 'admin') {

            return true;

        }

        if(is_array($roles)){

            foreach($roles as $need_role){

                if($this->checkIfUserHasRole($need_role)) {

                    return true;

                }

            }

        }else{

            return $this->checkIfUserHasRole($roles);

        }

        return false;

    }

    private function getUserRole()

    {

        return $this->role()->getResults();

    }

    private function checkIfUserHasRole($need_role)

    {     
   
        return (strtolower($need_role)==strtolower($this->have_role->title)) ? true : false;

    }
    
}
