<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testtype extends Model
{
    protected $table ='test_type';
    protected $fillable =['name'];
}
