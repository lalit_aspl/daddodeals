<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table ='survey';

    protected $fillable =['title','testtype_id','category_id','subcategory_id','user_id' ,'description'];


    public function questions()
    {     
       return $this->hasMany(Question::class);         
    }


    public function category()
    {
        return $this->hasOne('App\Category','id', 'category_id');  
    }

    public function subcategory()
    {
        return $this->hasOne('App\Subcategory','id', 'subcategory_id');  
    }

    public function type()
    {
        return $this->hasOne('App\Testtype','id', 'testtype_id');  
    }
    
}
