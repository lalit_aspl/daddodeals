<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table ='subcategory';
    protected $fillable =['name','category_id'];

    public function category(){
        return $this->belongsTo(Category::class)->select('id','name');
    }

    public function survey(){
        return $this->hasMany('App\Survey','subcategory_id','id');
    }

}
