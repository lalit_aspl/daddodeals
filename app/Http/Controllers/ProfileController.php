<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use App\Http\Controllers\Traits\FileUploadTrait;
use Hash;
use Auth;

class ProfileController extends Controller
{
      use FileUploadTrait;

     public function profile()
     {  
         return view('admin.profile');
     }


     public function update(Request $request)
     {

        
          $id  = $request->id;

          $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:mysql.users,email,'.$id,
            'phone' => 'required|unique:mysql.users,phone,'.$id,
           ]);

           $vaild = Validator::make($request->all(), [     
            'name' => 'required|max:255',      
            'email' => 'required|unique:mysql.users,email,'.$id,     
            'password' => 'required|min:6',    
            'phone' => 'required|unique:mysql.users,phone,'.$id,        
            ]);


            if(!empty($request->images)){
              
                $request['image'] = $this->profileimage($request);
                 
                    $ud = User::where('id', $id)->first();
                     if(!empty($ud['image']))
                     {
                        unlink(public_path('uploads/profile/thumb/') . $ud['image']);
                        unlink(public_path('uploads/profile/') . $ud['image']);
                     }
        
                }



                
            if(empty($request['password'])  )
            {
                if ($v->fails()) {
                    return redirect()->back()->withErrors($v)->withInput();
                } else {
                    User::where('id',$id)->update($request->except('_token','password','uploads','images'));
                }
            }else{

                if ($vaild->fails()) {
                    return redirect()->back()->withErrors($vaild)->withInput();
                } else {
                     $request['password'] = Hash::make($request['password']); 
                     User::where('id', Auth::id())->update($request->except('_token','images'));
                }

            }       

            return redirect()->back()->with('message', 'Profile Update Sucessully');
     }
}
