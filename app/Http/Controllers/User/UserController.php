<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Survey;
use App\Question;
use Auth;

class UserController extends Controller
{
    public function index($id=NULL){        
        $tests = Survey::with('type')->where('testtype_id' , $id)->get();              
        return view('users.tests.index',compact('tests'));
    }

    public function exam($id)
    {
       $questions =  Question::where('survey_id' , $id)->first();  
       if(empty( $questions)) 
       {
            return redirect()->back()->with('message','No Test Found');
       }       
       return view('users.tests.exam',compact('questions'));
    }
   
}
