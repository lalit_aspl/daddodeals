<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Config;

class MobileLoginController extends Controller
{
    public function index()
    {
        Session::forget('otpdata');
        return view('auth.mobilelogin');
    }



    public function generateotp(Request $request)
    {
        $checklogin =  User::where(['phone' => $request['phone'],'login_status' => 1])->first();        
        if(!empty($checklogin))
        {
            return back()->with('message' ,'You are Logged in another System please logout first');
        }


        $data =  User::where(['phone' => $request['phone'],'status' => 1])->first();
        $phone =  $request['phone'];
        
        if(!empty($data))
        {
             User::where('phone' , $request['phone'])->update(['otp' =>  rand(100000,999999)]);
             $userdata =    User::where('phone' , $request['phone'])->first();
             $time =  date('H:i:s', strtotime( $data['updated_at']));                
             

             $otp   = 'Dear '.$userdata['name'].' daffodilsstudy.com Please use this OTP to Login'.'('.$userdata['otp'].')';
              $urls = 'http://sms.paragalaxy.com/smpp_api/sms?token='.config('app.token').'&To='.urlencode($request['phone']).'&Text='.urlencode($otp);
        
          
             $ch = curl_init();
             curl_setopt($ch, CURLOPT_URL,  $urls);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
             $contents = curl_exec ($ch);
             if($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                curl_getinfo($curl, CURLINFO_HTTP_CODE); die();
            }
             curl_close ($ch);           
             
             
             $message = "You will receive OTP on phone  to login your account";
             return view('Auth.verify',compact('phone' ,'message' ,'time'));
             
        } else {
            return back()->with('message' ,'Please Enter the Vaild Mobile no');
        }
        
    }


    public function verifyotp(Request $request)
    {
      
        $difference = strtotime( date("H:i:s") ) - strtotime($request['time'] );
        //echo $difference; die();
        $data =  User::where(['phone' => $request['phone'] , 'otp' => $request['otp']])->where('otp' ,'!=', 0)->whereNotNull('otp')->first();
        
        if(isset($data) && !empty($data))
        {     
           
          if(isset($difference) and $difference <= 400){
            $user  = User::where(['phone' => $request['phone'] , 'otp' => $request['otp']])
            ->update(['last_login_at' => Carbon::now()->toDateTimeString(),'otp' => null ,'login_status' => 1]);
            Auth::loginUsingId($data->id);   
            echo "sucess";    
            //return redirect('/');
          }else{
            Session::forget('otpdata');
            echo 'Otp Expire please try again';
          }

        }else{
            Session::forget('otpdata');
            //redirect(Request::url())->with('message','Wrong Otp please try again');
           // return \App::make('redirect')->refresh()->with('message', 'Thank you,!');

            echo 'Wrong Otp please try again';
        }
    }

}
