<?php
namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;


trait FileUploadTrait
{

    /**
     * File upload trait used in controllers to upload files
     */
    


   
    public function profileimage(Request $request)
    {
      
        $image = $request->file('images');
        $name  =  $image->getClientOriginalName();
        $input['imagename'] = date('YmdHs').time().$name;
        $size = $image->getSize();
        $destinationPath = public_path('uploads/profile/thumb');       
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);        
         
        $destinationPath = public_path('/uploads/profile');
        $image->move($destinationPath, $input['imagename']);           
      
         return  $input['imagename'];        
    
    }


    public function audio(Request $request)
    {
        $file  = $request->file('correct_answers');
        $name  =  $file->getClientOriginalName();
         $ext  =  '.'.$file->getClientOriginalExtension();
        $filename = date('YmdHs').time().$ext;     
            $path = public_path().'/uploads/audio';
            $file->move($path, $filename);
        return  $filename ;  
    }
   
}
