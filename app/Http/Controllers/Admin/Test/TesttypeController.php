<?php

namespace App\Http\Controllers\Admin\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Testtype;

class TesttypeController extends Controller
{
    public function index()
    {    
        $type = Testtype::get();
        return view('admin.survey.testtype.index',compact('type'));
    }

    public function create()
    {    
        $edit = Testtype::get()->toArray();
        return view('admin.survey.testtype.create');
    }

    public function store(Request $request)
    {    
        Testtype::create($request->all());
        return redirect('admin/test/type/index')->with('message','Create Sucessfully');
    }

    public function edit( $id)
    {    
        $edit = Testtype::where('id' , $id)->first();
        return view('admin.survey.testtype.create',compact('edit'));
    }

    public function update(Request $request,$id)
    {    
        $edit = Testtype::where('id' , $id)->update($request->except('_token'));
        return redirect('admin/test/type/index')->with('message','Update Sucessfully');
    }

    public function delete($id)
    {    
        Testtype::where('id' , $id)->delete();
        return redirect('admin/test/type/index')->with('message','Delete Sucessfully');
    }
}
