<?php

namespace App\Http\Controllers\Admin\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subcategory;
use App\Category;
use Validator;

class SubcategoryController extends Controller
{
    public function index()
    {   
        $data = Subcategory::with('category')->get();
        //echo "<pre>"; print_r($data->toArray()); die();
        return view('admin.survey.subcategory.index',compact('data'));
    }

    public function create(Request $request)
    {   
        
        $data = Category::pluck('name', 'id')->toArray();        
        return view('admin.survey.subcategory.create',compact('data'));
        
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [            
            'name' => 'required',
            'category_id' => 'required',
        ], 
        [  
           'category_id.required'    => 'The Category  field is required.', 
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
        Subcategory::create($request->all());
        return redirect('/admin/test/subcategory')->with('message','SubCategory Create Sucessfully');
        }
    }

    public function edit($id)
    {
         $edit = Subcategory::where('id' , $id)->first();
         $data = Category::pluck('name', 'id');         
         return view('admin.survey.subcategory.create',compact('data','edit'));
    }

    public function update(Request $request ,$id)
    {
        $validator = Validator::make($request->all(), [            
            'name' => 'required',
            'category_id' => 'required',
        ], 
        [  
           'category_id.required'    => 'The Category  field is required.', 
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
         Subcategory::where('id' , $id)->update($request->except('_token'));        
         return redirect('/admin/test/subcategory')->with('message','SubCategory Update Sucessfully');
        }
    }

    public function delete($id)
    {
       //  Subcategory::where('id' , $id)->delete();    
         $data = Subcategory::findOrFail($id);
         $data->survey()->leftJoin('questions','questions.survey_id', '=', 'survey.id')->delete(); 
         $data->delete();     
         return redirect('/admin/test/subcategory')->with('message','SubCategory Delete Sucessfully');
    }
}
