<?php

namespace App\Http\Controllers\Admin\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Survey;
use Auth;
use App\Question;
use App\Category;
use App\Subcategory;
use Validator;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Testtype;

class SurveyController extends Controller
{
    use FileUploadTrait;

    public function index()
    {   
        $surveys = Survey::with('category')->with('subcategory')->with('type')->get();      
        return view('admin.survey.index',compact('surveys'));
    }

    public function new()
    {
        $data = Category::pluck('name','id');
        $type = Testtype::pluck('name','id');
        return view('admin.survey.new',compact('data' ,'type'));
    }   

    public function save(Request $request)
    {
       
        $validator = Validator::make($request->all(), [            
            'title' => 'required',
            'category_id'  => 'required',
            'subcategory_id'  => 'required',
            'testtype_id'  =>'required',
        ] , 
        [   
            'category_id.required'    => 'The Category  field is required.',
            'subcategory_id.required'    => 'The Subcategory  field is required.',
            'testtype_id.required'    => 'The Type  field is required.',
           
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
         
         $request['user_id'] = Auth::id();
        Survey::create($request->all());
        return redirect('/admin/survey/index')->with('message','Create Sucessfully');
        }
    }

    public function edit($id)
    {  
        
       $edit =  Survey::with('subcategory')->where('id' ,$id)->first();
       $data = Category::pluck('name','id');
       $type = Testtype::pluck('name','id');
       
       return view('admin.survey.new' ,compact('edit','data','type'));
    }
    
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [            
            'title' => 'required',
            'category_id'  => 'required',
            'subcategory_id'  => 'required',
        ] , 
        [   
            'category_id.required'    => 'The Category  field is required.',
            'subcategory_id.required'    => 'The Subcategory  field is required.',
           
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

        Survey::where('id' , $id)->update($request->except('_token'));
        return redirect('/admin/survey/index')->with('message','Update Sucessfully');
        }
    }


    public function subcat(Request $request)
    {
  
        $data = Subcategory::where('category_id',$request['id'])->get()->toArray();
        if(!empty($data)){
            foreach($data as $dts){
              echo "<option value =".$dts['id']."> ".$dts['name']." </option>";
            }
        
        }else{ echo "<option value =''>"."No date Found". "</option>"; }
    }


    
    public function survey(Request $request , $id)
    {        
         $survey  = Survey::with('questions')->where('id' , $id)->first();         
         return view('admin.survey.question.create',compact('survey'));      
    }


    public function deletesurvey(Request $request , $id)
    {        
      
         $survey  = Survey::where('id' , $id)->delete();  
          Question::where('survey_id', $id)->delete(); 
        //   die('');      
         return redirect('/admin/survey/index');      
    }
    



    public function savequestion(Request $request , $id)
    {    
        $request['user_id'] = Auth::id();        
        $request['survey_id'] =  $id;
        if($request['question_type'] == 'radio')
        {
            $request['option_name'] = implode(',', $request['option_name']);

        }elseif($request['question_type'] == 'checkbox'){

           $request['option_name'] = implode(',', $request['option_name']);
        }else{
            $request['option_name'] = null;
        }
       
          Question::create($request->all());
        return redirect()->back();
       
    }

     public function editquestion($id)
     {   
        $question = Question::where('id' , $id)->first();
        return view('admin.survey.question.edit',compact('question'));
     }

     public function updatequestion(Request $request , $id)
     {
        if ($request->hasFile('correct_answer')) {         
         $request['correct_answer'] = $this->audio($request);     
        }   
         Question::where('id' , $id)->update($request->except('_token'));
         return redirect()->back();
     }




     public function deletequestion($id)
     {
        Question::where(['id' => $id,'user_id' => Auth::id()])->delete();
        return redirect()->back();
     }
}
