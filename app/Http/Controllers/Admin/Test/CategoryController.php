<?php

namespace App\Http\Controllers\Admin\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {    
        $category = Category::orderBy('name','asc')->get();      
        return view('admin.survey.category.index',compact('category'));
    }

    public function create()
    {
        return view('admin.survey.category.create');
    }

    public function store(Request $request)
    {
        Category::create($request->all());
        return redirect('admin/test/category');
    }

    public function edit($id)
    {
       $edit = Category::where('id' , $id)->first();
       return view('admin.survey.category.create',compact('edit'));
    }

    public function update(Request $request, $id)
    {
        Category::where('id' , $id)->update($request->except('_token'));
        return redirect('admin/test/category');
    }

    public function delete($id)
    {              
          $data = Category::findOrFail($id);
          $data->survey()->leftJoin('questions','questions.survey_id', '=', 'survey.id')->delete();        
          $data->subcategory()->delete();
                
         $data->delete();
        return redirect('admin/test/category');
    }
}
