<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DataTables;
use DB;
class UserController extends Controller
{
    public function index(Request $request)
    {
       
        if ($request->ajax()) {
            
            $data = User::select('id','name','email','phone',DB::raw("DATE_FORMAT(last_login_at, '%d-%b-%Y %r') as last_login"))->where('role_id',2)->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-id = "'.$row->id.'" class="btn btn-xs btn-danger deletuser">Delete</a>';
                           //$btn = '<a href="'.url('/users/profile/').'/'.$row->id .'" data-id = "'.$row->id.'" class="btn btn-xs btn-danger deletuser">Detail</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
                    
        } 
        return view('admin.user.index');
    }

    public function delete($id)
    {
      User::destroy($id); 
    }
}
