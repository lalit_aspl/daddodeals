<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table ='questions';

    protected $fillable =['survey_id' ,'user_id' ,'title','question_type','option_name','correct_answer'];
}
