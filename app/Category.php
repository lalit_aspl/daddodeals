<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table ='category';

    protected $fillable =['name'];

    public function subcategory()
    {
        return $this->hasMany('App\Subcategory','category_id' , 'id');
    }

    public function survey()
    {
        return $this->hasMany('App\Survey','category_id' , 'id');
    }
    
}
